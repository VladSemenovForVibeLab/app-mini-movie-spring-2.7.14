package com.svf.filmapp.service;

import com.svf.filmapp.domain.Film;
import com.svf.filmapp.dto.FilmDTO;
import com.svf.filmapp.exception.ResourceNotFoundException;
import com.svf.filmapp.repository.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilmService {
    @Autowired
    private FilmRepository filmRepository;

    public List<Film> getAllFilms() {
        return filmRepository.findAll();
    }

    public void createNewFilm(Film film) {
        filmRepository.save(film);
    }

    public Film findFilmById(Long id) {
        return filmRepository.findById(id)
                .orElseThrow(()->
                       new ResourceNotFoundException("Фильм не найден по данному id "+id));
    }

    public void deleteFilmById(Long id) {
        Film film = findFilmById(id);
        filmRepository.delete(film);
    }

    public void updateFilm(Long id, FilmDTO filmDTO) {
        Film film = findFilmById(id);

        film.setFilmName(filmDTO.getFilmName());
        film.setReleaseYear(filmDTO.getReleaseYear());
        film.setImdbRating(filmDTO.getImdbRating());
        film.setProducer(filmDTO.getProducer());
        film.setGenre(filmDTO.getGenre());

        filmRepository.save(film);
    }

}
