package com.svf.filmapp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.svf.filmapp.domain.Film;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmDTO {

    private Long id;
    @NotBlank(message = "Название не должно быть пустым")
    @Size(min = 2,max = 50,message = "Необходимо ввести название от {min} до {max} символов")
    private String filmName;
    @NotBlank(message = "Год релиза не может быть пустым")
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy")
    private String releaseYear;
    private Double imdbRating;
    @NotBlank(message = "Продюсер фильма не может быть пустым")
    @Size(min = 2,max = 20,message = "Размер имени продюсера может быть от {min} до {max} количества символов")
    private String producer;
    @NotBlank(message = "Поле не может быть пустым")
    @Size(min = 2,max = 25,message = "Данное поле должно быть от {min} до {max} символов")
    private String genre;

    public FilmDTO(Film film) {
        this.id = film.getId();
        this.filmName = film.getFilmName();
        this.releaseYear = film.getReleaseYear();
        this.imdbRating = film.getImdbRating();
        this.producer = film.getProducer();
        this.genre = getGenre();
    }
}
