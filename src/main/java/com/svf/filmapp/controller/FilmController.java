package com.svf.filmapp.controller;

import com.svf.filmapp.domain.Film;
import com.svf.filmapp.dto.FilmDTO;
import com.svf.filmapp.service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("films")
@CrossOrigin(origins = "http://localhost:5173")
public class FilmController {
    @Autowired
    private FilmService filmService;

    @GetMapping
    public ResponseEntity<List<Film>> listAllFilms(){
        List<Film> films = filmService.getAllFilms();
        return ResponseEntity.ok(films);
    }
    @PostMapping
    public ResponseEntity<Map<String,String>> createFilm(@Valid @RequestBody Film film){
        filmService.createNewFilm(film);
        Map<String,String> map = new HashMap<>();
        map.put("message","Фильм успешно создан");
        map.put("status","true");
        return new ResponseEntity<>(map, HttpStatus.CREATED);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Film> getFilmById(@PathVariable("id") Long id){
        return ResponseEntity.ok(filmService.findFilmById(id));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteFile(@PathVariable("id") Long id){
        filmService.deleteFilmById(id);
        String message = "Фильм успешно удален!";
        return new ResponseEntity<>(message,HttpStatus.OK);
    }
    @PutMapping("/{id}")
    public ResponseEntity<String> updateFilm(@PathVariable("id") Long id,
                                             @Valid @RequestBody FilmDTO filmDTO){
        filmService.updateFilm(id,filmDTO);
        String message = "Фильм успешно обновлён!";
        return new ResponseEntity<>(message,HttpStatus.OK);
    }

}
