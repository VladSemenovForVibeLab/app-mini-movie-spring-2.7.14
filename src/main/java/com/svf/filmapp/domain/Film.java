package com.svf.filmapp.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Film {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private Long id;
    @NotBlank(message = "Название не должно быть пустым")
    @Size(min = 2,max = 50,message = "Необходимо ввести название от {min} до {max} символов")
    @Column(nullable = false,length = 50)
    private String filmName;
    @NotBlank(message = "Год релиза не может быть пустым")
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy")
    private String releaseYear;
    private Double imdbRating;
    @NotBlank(message = "Продюсер фильма не может быть пустым")
    @Size(min = 2,max = 20,message = "Размер имени продюсера может быть от {min} до {max} количества символов")
    @Column(nullable = false,length = 20)
    private String producer;
    @NotBlank(message = "Поле не может быть пустым")
    @Size(min = 2,max = 25,message = "Данное поле должно быть от {min} до {max} символов")
    @Column(nullable = false,length = 25)
    private String genre;
}
